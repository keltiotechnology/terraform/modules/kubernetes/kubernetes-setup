## Cluster Issuer CRD Deployment ##
resource "null_resource" "cluster_issuer_crd" {
  provisioner "local-exec" {
    command = "kubectl apply -f ${var.certmgr_crd_manifest_url}"
  }
}

## Cluster Issuer Deployment ##
resource "null_resource" "cluster_issuer_manifest" {
  for_each = var.issuers

  provisioner "local-exec" {
    command = "echo '${templatefile("${path.module}/templates/cluster-issuer.tpl", {
      issuer_env        = each.value.env
      issuer_admin_mail = each.value.admin_mail
      issuer_server_url = each.value.server_url
    })}' | kubectl apply -f -"
  }
  depends_on = [null_resource.cluster_issuer_crd, helm_release.cert-manager]
}