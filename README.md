<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Kubernetes Cluster Config

Terraform module which will configure a Kubernetes cluster with the following deployments:

- Nginx Ingress Controller
- Cert Manager
- a Cluster Issuer resource

# 

## Module usage

```hcl
provider "helm" {
  kubernetes {
    config_path = var.kubeconfig_path
  }
}

provider "null" {}

module "kubernetes-setup" {
  source = "../.."

  ## Common ##
  create_namespace = var.create_namespace

  ## NGINX Chart configs ##
  nginx_helm_repository       = var.nginx_helm_repository
  nginx_helm_chart            = var.nginx_helm_chart
  nginx_helm_template_version = var.nginx_helm_template_version
  nginx_helm_release_name     = var.nginx_helm_release_name
  nginx_helm_namespace        = var.nginx_helm_namespace
  nginx_additional_sets       = var.nginx_additional_sets

  ## Cert Manager configs ##
  certmgr_helm_repository       = var.certmgr_helm_repository
  certmgr_helm_chart            = var.certmgr_helm_chart
  certmgr_helm_template_version = var.certmgr_helm_template_version
  certmgr_helm_release_name     = var.certmgr_helm_release_name
  certmgr_helm_namespace        = var.certmgr_helm_namespace
  certmgr_additional_sets       = var.certmgr_additional_sets

  ## Cluster Issuer ##
  issuers = var.issuers
}
```
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | >= 2.3.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 3.1.0 |

## Resources

| Name | Type |
|------|------|
| [helm_release.cert-manager](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.ingress-controller](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [null_resource.cluster_issuer_crd](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.cluster_issuer_manifest](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.3.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_certmgr_additional_sets"></a> [certmgr\_additional\_sets](#input\_certmgr\_additional\_sets) | Additional chart values definition | `map(any)` | n/a | yes |
| <a name="input_certmgr_crd_manifest_url"></a> [certmgr\_crd\_manifest\_url](#input\_certmgr\_crd\_manifest\_url) | Custom resource definition manifest URL for Cert Manager | `string` | `"https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.crds.yaml"` | no |
| <a name="input_certmgr_helm_chart"></a> [certmgr\_helm\_chart](#input\_certmgr\_helm\_chart) | Cert Manager helm chart name | `string` | `"cert-manager"` | no |
| <a name="input_certmgr_helm_namespace"></a> [certmgr\_helm\_namespace](#input\_certmgr\_helm\_namespace) | Kubernetes namespace in which cert-manager helm chart will be installed | `string` | `"cert-manager"` | no |
| <a name="input_certmgr_helm_release_name"></a> [certmgr\_helm\_release\_name](#input\_certmgr\_helm\_release\_name) | Cert Manager helm release name | `string` | `"cert-manager"` | no |
| <a name="input_certmgr_helm_repository"></a> [certmgr\_helm\_repository](#input\_certmgr\_helm\_repository) | Cert Manager helm repository | `string` | `"https://charts.jetstack.io"` | no |
| <a name="input_certmgr_helm_template_version"></a> [certmgr\_helm\_template\_version](#input\_certmgr\_helm\_template\_version) | Cert Manager chart version which will be installed | `string` | `"v1.5.4"` | no |
| <a name="input_create_namespace"></a> [create\_namespace](#input\_create\_namespace) | Set whether to create or not a namespace if it doesn't already exist | `bool` | `true` | no |
| <a name="input_issuers"></a> [issuers](#input\_issuers) | Cluster issuers list to deploy | `map(any)` | n/a | yes |
| <a name="input_nginx_additional_sets"></a> [nginx\_additional\_sets](#input\_nginx\_additional\_sets) | Additional chart values definition | `map(any)` | n/a | yes |
| <a name="input_nginx_helm_chart"></a> [nginx\_helm\_chart](#input\_nginx\_helm\_chart) | Ingress Nginx helm chart name | `string` | `"ingress-nginx"` | no |
| <a name="input_nginx_helm_namespace"></a> [nginx\_helm\_namespace](#input\_nginx\_helm\_namespace) | Kubernetes namespace in which the nginx helm chart will be installed | `string` | `"default"` | no |
| <a name="input_nginx_helm_release_name"></a> [nginx\_helm\_release\_name](#input\_nginx\_helm\_release\_name) | Ingress Nginx helm release name | `string` | `"ingress-controller"` | no |
| <a name="input_nginx_helm_repository"></a> [nginx\_helm\_repository](#input\_nginx\_helm\_repository) | Ingress Nginx helm repository | `string` | `"https://kubernetes.github.io/ingress-nginx"` | no |
| <a name="input_nginx_helm_template_version"></a> [nginx\_helm\_template\_version](#input\_nginx\_helm\_template\_version) | Nginx chart version which will be installed | `string` | `"4.0.3"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cert-manager-status"></a> [cert-manager-status](#output\_cert-manager-status) | Outputs helm cert-manager chart deployment state |
| <a name="output_ingress-controller-status"></a> [ingress-controller-status](#output\_ingress-controller-status) | Outputs helm ingress-nginx chart deployment state |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->