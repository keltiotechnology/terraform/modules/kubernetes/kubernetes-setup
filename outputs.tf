output "ingress-controller-status" {
  value       = helm_release.ingress-controller.metadata
  description = "Outputs helm ingress-nginx chart deployment state"
}

output "cert-manager-status" {
  value       = helm_release.cert-manager.metadata
  description = "Outputs helm cert-manager chart deployment state"
}