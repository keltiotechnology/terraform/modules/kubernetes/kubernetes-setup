apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: cluster-issuer-${issuer_env}
spec:
  acme:
    email: ${issuer_admin_mail}
    server: ${issuer_server_url}
    privateKeySecretRef:
      name: cluster-issuer-secret-${issuer_env}
    solvers:
    - http01:
        ingress:
          class: nginx