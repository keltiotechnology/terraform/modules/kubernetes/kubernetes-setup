provider "helm" {
  kubernetes {
    config_path = var.kubeconfig_path
  }
}

provider "null" {}

module "kubernetes-setup" {
  source = "../.."

  ## Common ##
  create_namespace = var.create_namespace

  ## NGINX Chart configs ##
  nginx_helm_repository       = var.nginx_helm_repository
  nginx_helm_chart            = var.nginx_helm_chart
  nginx_helm_template_version = var.nginx_helm_template_version
  nginx_helm_release_name     = var.nginx_helm_release_name
  nginx_helm_namespace        = var.nginx_helm_namespace
  nginx_additional_sets       = var.nginx_additional_sets

  ## Cert Manager configs ##
  certmgr_helm_repository       = var.certmgr_helm_repository
  certmgr_helm_chart            = var.certmgr_helm_chart
  certmgr_helm_template_version = var.certmgr_helm_template_version
  certmgr_helm_release_name     = var.certmgr_helm_release_name
  certmgr_helm_namespace        = var.certmgr_helm_namespace
  certmgr_additional_sets       = var.certmgr_additional_sets

  ## Cluster Issuer ##
  issuers = var.issuers
}