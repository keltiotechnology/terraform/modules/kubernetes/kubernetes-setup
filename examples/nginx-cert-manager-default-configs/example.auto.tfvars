## Common ##
create_namespace = true

## NGINX Chart configs ##
nginx_helm_repository       = "https://kubernetes.github.io/ingress-nginx"
nginx_helm_chart            = "ingress-nginx"
nginx_helm_template_version = "4.0.3"
nginx_helm_release_name     = "ingress-nginx"
nginx_helm_namespace        = "ingress-nginx"
nginx_additional_sets = {
  set1 = {
    name  = "controller.kind"
    value = "DaemonSet"
  }
}

## Cert Manager configs ##
certmgr_helm_repository       = "https://charts.jetstack.io"
certmgr_helm_chart            = "cert-manager"
certmgr_helm_template_version = "v1.5.4"
certmgr_helm_release_name     = "cert-manager"
certmgr_helm_namespace        = "cert-manager"
certmgr_crd_manifest_url      = "https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.crds.yaml"
certmgr_additional_sets = {
  set1 = {
    name  = "prometheus.enabled"
    value = "false"
  }
}

## Cluster Issuers ##
issuers = {
  issuer_staging = {
    env        = "staging"
    admin_mail = "admin@mydomain.fr"
    server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
  }
  issuer_prod = {
    env        = "prod"
    admin_mail = "admin@mydomain.fr"
    server_url = "https://acme-v02.api.letsencrypt.org/directory"
  }
}