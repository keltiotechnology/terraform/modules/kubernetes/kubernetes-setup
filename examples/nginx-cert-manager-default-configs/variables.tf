## Common ##
variable "create_namespace" {
  type        = bool
  description = "Set whether to create or not a namespace if it doesn't already exist"
  default     = true
}

## Kubernetes Config
variable "kubeconfig_path" {
  type        = string
  description = "Full path to the kubeconfig file"
  default     = "~/.kube/config"
}

## NGINX Chart configs ##
variable "nginx_helm_release_name" {
  type        = string
  description = "Ingress Nginx helm release name"
  default     = "ingress-controller"
}

variable "nginx_helm_repository" {
  type        = string
  description = "Ingress Nginx helm repository"
  default     = "https://kubernetes.github.io/ingress-nginx"
}

variable "nginx_helm_chart" {
  type        = string
  description = "Ingress Nginx helm chart name"
  default     = "ingress-nginx"
}

variable "nginx_helm_template_version" {
  type        = string
  description = "Nginx chart version which will be installed"
  default     = "4.0.3"
}

variable "nginx_helm_namespace" {
  type        = string
  description = "Kubernetes namespace in which the nginx helm chart will be installed"
  default     = "default"
}

variable "nginx_additional_sets" {
  type        = map(any)
  description = "Additional chart values definition"
}

## Cert Manager configs ##
variable "certmgr_helm_release_name" {
  type        = string
  description = "Cert Manager helm release name"
  default     = "cert-manager"
}

variable "certmgr_helm_repository" {
  type        = string
  description = "Cert Manager helm repository"
  default     = "https://charts.jetstack.io"
}

variable "certmgr_helm_chart" {
  type        = string
  description = "Cert Manager helm chart name"
  default     = "cert-manager"
}

variable "certmgr_helm_template_version" {
  type        = string
  description = "Cert Manager chart version which will be installed"
  default     = "v1.5.4"
}

variable "certmgr_helm_namespace" {
  type        = string
  description = "Kubernetes namespace in which cert-manager helm chart will be installed"
  default     = "cert-manager"
}

variable "certmgr_crd_manifest_url" {
  type        = string
  description = "Custom resource definition manifest URL for Cert Manager"
  default     = "https://github.com/jetstack/cert-manager/releases/download/v1.5.4/cert-manager.crds.yaml"
}

variable "certmgr_additional_sets" {
  type        = map(any)
  description = "Additional chart values definition"
}

## Cluster Issuer ##
variable "issuers" {
  type        = map(any)
  description = "Cluster issuers list to deploy"
}