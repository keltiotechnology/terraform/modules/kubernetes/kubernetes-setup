## Cert Manager Helm release
resource "helm_release" "cert-manager" {
  depends_on       = [helm_release.ingress-controller]
  name             = var.certmgr_helm_release_name
  namespace        = var.certmgr_helm_namespace
  create_namespace = var.create_namespace
  chart            = var.certmgr_helm_chart
  repository       = var.certmgr_helm_repository
  version          = var.certmgr_helm_template_version

  dynamic "set" {
    for_each = var.certmgr_additional_sets

    iterator = config
    content {
      name  = config.value.name
      value = config.value.value
    }
  }
}

## Ingress Nginx Helm release
resource "helm_release" "ingress-controller" {
  name             = var.nginx_helm_release_name
  namespace        = var.nginx_helm_namespace
  create_namespace = var.create_namespace
  chart            = var.nginx_helm_chart
  repository       = var.nginx_helm_repository
  version          = var.nginx_helm_template_version

  dynamic "set" {
    for_each = var.nginx_additional_sets

    iterator = config
    content {
      name  = config.value.name
      value = config.value.value
    }
  }
}

## External DNS Helm release
resource "helm_release" "external_dns" {
  name             = var.extdns_helm_release_name
  namespace        = var.extdns_helm_namespace
  create_namespace = var.create_namespace
  chart            = var.extdns_helm_chart
  repository       = var.extdns_helm_repository
  version          = var.extdns_helm_template_version

  dynamic "set" {
    for_each = var.extdns_additional_sets

    iterator = config
    content {
      name  = config.value.name
      value = config.value.value
    }
  }
}
